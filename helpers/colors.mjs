import Color from 'color';
import Handlebars from 'handlebars';

function color(object, index) {
  return Color(object[index]);
}

export default {
  color(object, index) {
    return new Handlebars.SafeString(`'${color(object, index).hex()}'`);
  },
  alpha(object, index, percent) {
    return new Handlebars.SafeString(`'${color(object, index).alpha(percent).hexa()}'`);
  },
  lighten(object, index, percent) {
    return new Handlebars.SafeString(`'${color(object, index).lighten(percent).hex()}'`);
  },
  darken(object, index, percent) {
    return new Handlebars.SafeString(`'${color(object, index).lighten(percent * -1).hex()}'`);
  }
}