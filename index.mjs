#!/usr/bin/env node

import 'colors';

import { init } from './libs/init.mjs';
import { build } from './libs/build.mjs';
import { watch } from './libs/watch.mjs';

import { args, argv, command } from './libs/cli.mjs';

switch(command) {
  case 'init':
    await init(argv);
    break;
  case 'build':
    if(argv.watch) {
      await watch(argv);
    } else {
      await build(argv);
    }
    break;
  default:
    args.showHelp();
}