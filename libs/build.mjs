import path from 'path';
import { readFile, writeFile } from 'fs/promises';

import Handlebars from 'handlebars';
import toml from 'toml';

import { DIRNAME } from './constants.mjs';
import { importModules, toList } from './utils.mjs';

export async function build({ template='', output='', colors=[ ], helpers=[ ], verbose=false }) {
  helpers.push(path.resolve(DIRNAME, './helpers/**/*.mjs'));

  const colorsModules = (await toList(importModules(colors, { reload: true }))).map(module => module.default);
  const helpersModules = (await toList(importModules(helpers, { reload: true }))).map(module => module.default);

  const context = { };

  for(const module of colorsModules) {
    for(const key in module) {
      context[key] = module[key];
    }
  }

  for(const module of helpersModules) {
    for(const helper in module) {
      Handlebars.registerHelper(helper, module[helper]);
    }
  }

  let source = null;

  try {
    source = await readFile(template);
  } catch {
    console.error(`Cannot read template.`.red);
  }

  let render = null;

  try {
    render = Handlebars.compile(source.toString());
  } catch(error) {
    throw new Error(`Handlebars ${error}`.red);
  }

  let json = null;

  try {
    json = toml.parse(render(context));
  } catch(error) {
    console.error(`TOML Parse ${error}.`.red);
  }

  try {
    if(json) {
      await writeFile(output, JSON.stringify(json, null, 2));
    }
  } catch(error) {
    console.error(`Write ${error}.`.red);
  }
}
