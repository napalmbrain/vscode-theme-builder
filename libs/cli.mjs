import yargs from 'yargs';

export const args = yargs(process.argv.slice(2))
  .scriptName('vscode-theme-builder'.magenta)
  .command('init', 'ititialize defaults'.green, {
    'directory': {
      alias: 'd',
      default: './build',
      describe: 'directory to initialize',
      type: 'string'
    }
  })
  .command('build', 'build a theme'.green, {
    'colors': {
      alias: 'c',
      default: [ './build/colors' ],
      describe: 'colors directory',
      type: 'array'
    },
    'helpers': {
      alias: 'e',
      default: [ './build/helpers' ],
      describe: 'helpers directory',
      type: 'array'
    },
    'output': {
      alias: 'o',
      demandOption: true,
      describe: 'output file',
      type: 'string'
    },
    'template': {
      alias: 't',
      default: './build/templates/theme.mustache.toml',
      describe: 'template file',
      type: 'string'
    },
    'watch': {
      alias: 'w',
      default: false,
      describe: 'watch for changes',
      type: 'boolean'
    },
    'watch-directory': {
      alias: 'd',
      default: './build',
      describe: 'directory to watch',
      type: 'string'
    }
  });

export const argv = args.argv;
export const command = argv._.pop();
