import path from 'path';
import { globbyStream } from 'globby';
import { v4 } from 'uuid';

export async function* importModules(pattern, options={ reload: false }) {
  const files = globbyStream(pattern, options);
  for await(const file of files) {
    if(options.reload) {
      // XXX: This could be a memory leak.
      yield await import(`${path.resolve(file.toString())}?uuid=${v4()}`);
    } else {
      yield await import(path.resolve(file.toString()));
    }
  }
}

export async function toList(generator) {
  const result = [ ];
  for await(const item of generator) {
    result.push(item);
  }
  return result;
}