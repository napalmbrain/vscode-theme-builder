import watcher from 'node-watch';
import { build } from './build.mjs';

import { argv } from './cli.mjs';

export async function watch({ watchDirectory=null }) {
  console.info(`Watching ${watchDirectory} for changes.`.magenta);
  watcher(watchDirectory, { recursive: true }, async (event, name) => {
    console.info(event.green, name);
    try {
      await build(argv);
    } catch(error) {
      console.error(`${error}`.red);
    }
  });
}