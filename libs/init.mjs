import path from 'path';
import { mkdir, access, copyFile } from 'fs/promises';

import { DIRNAME } from './constants.mjs';

async function make(directory) {
  try {
    await access(directory);
    console.error(`${directory} already exists.`.red);
  } catch {
    await mkdir(directory, { recursive: true });
    console.info(`Created directory ${directory}.`.green);
  }
}

async function copy(source, destination) {
  try {
    await access(destination);
    console.error(`${destination} already exists.`.red);
  } catch {
    await copyFile(source, destination);
    console.info(`Wrote file ${destination}.`.green);
  }
}

export async function init({ directory='' }) {
  if(!directory.length) {
    console.error('Directory not provided.'.red);
    process.exit(1);
  }

  console.info(`Initializing directory: ${directory}`.magenta);

  const colors = `${directory}/colors`;
  const helpers = `${directory}/helpers`;
  const templates = `${directory}/templates`;

  await make(colors);
  await make(helpers);
  await make(templates);

  const template = 'theme.mustache.toml';
  const source = path.resolve(DIRNAME, `./templates/${template}`);
  const destination = `${templates}/${template}`;

  await copy(source, destination);
}